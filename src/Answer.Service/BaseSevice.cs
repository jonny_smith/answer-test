﻿using System;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Answer.Data;
using Answer.Domain;

namespace Answer.Service
{
    public abstract class BaseSevice
    {
        protected IAnswerContext Context;
        private readonly ModelValidation _validator;

        protected BaseSevice()
        {
            _validator = new ModelValidation();
        }

        protected BaseSevice(IAnswerContext context)
            : this()
        {
            Context = context;
        }

        protected void Validate<T>(T entity, IServiceResult result)
        {
            result.Update(_validator.Validate(entity));
        }

        protected async Task<int?> CommitChangesAsync(IServiceResult result)
        {
            if (result.HasErrors) return null;

            try
            {
                return await Context.CommitChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Trace.WriteLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Trace.WriteLine(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                throw;
            }
            catch (Exception exception)
            {
                result.AddException(exception);
            }

            return null;
        }

        public IQueryable<T> Query<T>() where T : class, IEntity
        {
            return Context.Query<T>();
        }
    }
}
