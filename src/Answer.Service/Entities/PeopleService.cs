﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Answer.Data;
using Answer.Domain.Data;

namespace Answer.Service.Entities
{
    public class PeopleService : BaseSevice
    {
        public PeopleService(IAnswerContext context)
            : base(context)
        {
        }

        public async Task<ServiceResult<Person>> Create(Person model)
        {
            var result = new ServiceResult<Person>();

            Validate(model, result);
            AddPerson(result, model);
            await AddColours(result, model);
            
            await CommitChangesAsync(result);

            return result;
        }

        public async Task<ServiceResult<Person>> Update(Person model)
        {
            var result = new ServiceResult<Person>();
            
            Validate(model, result);

            await GetAndUpdatePerson(result, model);
            await UpdateColours(result, model);
            await CommitChangesAsync(result);

            return result;
        }

        public async Task<ServiceResult<Person>> Delete(int id)
        {
            var result = new ServiceResult<Person>();

            await DeletePerson(result, id);
            await CommitChangesAsync(result);

            return result;
        }

        public Person TestMethod(int personId)
        {
            return Context.QueryWith<Person>(x => x.Colours).FirstOrDefault(x => x.PersonId == personId);
        }

        public async Task<IEnumerable<Person>> TestMethodAsync()
        {
            return await Context.QueryWith<Person>(x => x.Colours).Where(x => x.IsEnabled).ToListAsync();
        }

        private async Task DeletePerson(ServiceResult<Person> result, int id)
        {
            result.Entity = await Context.Query<Person>().Include(x => x.Colours).SingleOrDefaultAsync(x => x.PersonId == id);
            if (result.Entity == null)
            {
                result.AddModelError("Person not found.");
                return;
            }

            Context.Delete(result.Entity);
        }

        private void AddPerson(ServiceResult<Person> result, Person model)
        {
            if (result.HasErrors) return;

            result.Entity = new Person(model);
            Context.Create(result.Entity);
        }

        private async Task UpdateColours(ServiceResult<Person> result, Person model)
        {
            if (result.HasErrors) return;

            foreach (var colour in result.Entity.Colours.ToList())
            {
                result.Entity.Colours.Remove(colour);
            }

            await AddColours(result, model);
        }

        private async Task AddColours(ServiceResult<Person> result, Person model)
        {
            if (result.HasErrors) return;
            
            var selectedColourIds = model.Colours.Select(x => x.ColourId).ToList();
            var selectedColours = await Context.Query<Colour>().Where(x => selectedColourIds.Contains(x.ColourId)).ToListAsync();

            foreach (var selectedColour in selectedColours)
            {
                result.Entity.Colours.Add(selectedColour);
            }
        }

        private async Task GetAndUpdatePerson(ServiceResult<Person> result, Person model)
        {
            if (result.HasErrors) return;

            result.Entity = await Context.Query<Person>().Include(x => x.Colours).SingleOrDefaultAsync(x => x.PersonId == model.PersonId);

            if (result.Entity == null)
                result.AddModelError("Person not found.");

            result.Entity.Update(model);
        }

    }
}
