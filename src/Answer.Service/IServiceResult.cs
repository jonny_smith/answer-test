﻿using System;

namespace Answer.Service
{
    public interface IServiceResult
    {
        bool Success { get; set; }
        bool HasErrors { get; }
        void Update(ModelValidationResult validationResult);
        void AddException(Exception exception);
        void AddModelError(string errorMessage);
    }
}