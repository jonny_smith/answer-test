﻿using System.Threading;

namespace Answer.Mapper
{
    public interface IMapper
    {
        void Register();
    }
}