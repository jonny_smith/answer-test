﻿using System;
using System.Linq.Expressions;
using AutoMapper;

namespace Answer.Mapper
{
    public static class MapperExtension
    {
        public static T MapTo<T>(this object entity)
        {
            return AutoMapper.Mapper.Map<T>(entity);
        }

        public static T MapTo<T>(this object entity, Action<IMappingOperationOptions> opts)
        {
            return AutoMapper.Mapper.Map<T>(entity, opts);
        }

        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map, Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, config => config.Ignore());
            return map;
        }
    }
}