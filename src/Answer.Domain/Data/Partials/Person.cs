﻿namespace Answer.Domain.Data
{
    public partial class Person : IEntity
    {
        public Person(Person model) : this()
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            IsAuthorised = model.IsAuthorised;
            IsEnabled = model.IsEnabled;
        }

        public void Update(Person model)
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            IsAuthorised = model.IsAuthorised;
            IsEnabled = model.IsEnabled;
        }
    }
}
