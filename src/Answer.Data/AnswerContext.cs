﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Answer.Domain.Data;

namespace Answer.Data
{
    public class AnswerContext : IAnswerContext
    {
        private readonly TechTestEntities _entities;


        public AnswerContext(TechTestEntities entities)
        {
            _entities = entities;
            _entities.Configuration.ProxyCreationEnabled = false;
            _entities.Configuration.LazyLoadingEnabled = false;
        }

        public virtual T Attach<T>(T entity, Func<T, bool> predicate) where T : class
        {
            var exists = _entities.Set<T>().Local.FirstOrDefault(predicate);
            if (exists != null) return exists;
            _entities.Set<T>().Attach(entity);
            return entity;
        }

        public virtual T Create<T>(T entity) where T : class
        {
            return _entities.Set<T>().Add(entity);
        }

        public virtual T Delete<T>(T entity) where T : class
        {
            return _entities.Set<T>().Remove(entity);
        }

        public virtual IQueryable<T> Query<T>() where T : class
        {
            return _entities.Set<T>();
        }

        public virtual IQueryable<T> QueryWith<T>(params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            IQueryable<T> query = _entities.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public virtual T Find<T>(params object[] id) where T : class
        {
            return _entities.Set<T>().Find(id);
        }

        public virtual int CommitChanges()
        {
            return _entities.SaveChanges();
        }

        public virtual async Task<int> CommitChangesAsync()
        {
            return await _entities.SaveChangesAsync();
        }
    }
}
