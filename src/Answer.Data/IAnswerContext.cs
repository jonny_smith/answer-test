using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Answer.Data
{
    public interface IAnswerContext
    {
        T Attach<T>(T entity, Func<T, bool> predicate) where T : class;
        T Create<T>(T entity) where T : class;
        T Delete<T>(T entity) where T : class;
        IQueryable<T> Query<T>() where T : class;
        IQueryable<T> QueryWith<T>(params Expression<Func<T, object>>[] includeProperties) where T : class;
        T Find<T>(params object[] id) where T : class;
        int CommitChanges();
        Task<int> CommitChangesAsync();
    }
}