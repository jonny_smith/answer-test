﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Answer.Data
{
    public class MockAnswerContext : IAnswerContext
    {
        public T Attach<T>(T entity, Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public T Create<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public T Delete<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> QueryWith<T>(params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            throw new NotImplementedException();
        }

        public T Find<T>(params object[] id) where T : class
        {
            throw new NotImplementedException();
        }

        public int CommitChanges()
        {
            throw new NotImplementedException();
        }

        public Task<int> CommitChangesAsync()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Includes<T>(params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            throw new NotImplementedException();
        }
    }
}