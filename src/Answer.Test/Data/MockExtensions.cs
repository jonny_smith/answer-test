﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Answer.Domain.Data;
using Moq;

namespace Answer.Data
{
    public static class MockExtensions
    {
        public static void ToAsyncMock<T>(this Mock<IQueryable<T>> mockSet, IQueryable<T> query)
        {
            mockSet.As<IDbAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new MockDbAsyncEnumerator<T>(query.GetEnumerator()));

            mockSet
                .Setup(m => m.Provider)
                .Returns(new MockDbAsyncQueryProvider<T>(query.Provider));

            mockSet.Setup(m => m.Expression).Returns(query.Expression);
            mockSet.Setup(m => m.ElementType).Returns(query.ElementType);
            mockSet.Setup(m => m.GetEnumerator()).Returns(query.GetEnumerator()); 
        }

        public static void Setup<T>(this Mock<IAnswerContext> context, IQueryable<T> query) where T : class 
        {
            var mockSet = new Mock<IQueryable<T>>();

            mockSet.As<IDbAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new MockDbAsyncEnumerator<T>(query.GetEnumerator()));

            mockSet
                .Setup(m => m.Provider)
                .Returns(new MockDbAsyncQueryProvider<T>(query.Provider));

            mockSet.Setup(m => m.Expression).Returns(query.Expression);
            mockSet.Setup(m => m.ElementType).Returns(query.ElementType);
            mockSet.Setup(m => m.GetEnumerator()).Returns(query.GetEnumerator()); 
            
            context.Setup(x => x.Query<T>()).Returns(mockSet.Object);
            context.Setup(x => x.QueryWith(It.IsAny<Expression<Func<T, object>>>())).Returns(mockSet.Object);
        }
    }
}