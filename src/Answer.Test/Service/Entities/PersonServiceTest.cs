﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Answer.Data;
using Answer.Domain.Data;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace Answer.Service.Entities
{
    [TestFixture]
    public class PersonServiceTest
    {
        private PeopleService _peopleService;
        private Mock<IAnswerContext> _context;
        private readonly IQueryable<Person> _personList = new List<Person>
        {
            new Person { PersonId = 1, IsEnabled = false },
            new Person { PersonId = 2, IsEnabled = true },
            new Person { PersonId = 3, IsEnabled = true },
        }.AsQueryable();

        [SetUp]
        public void Setup()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _context = fixture.Freeze<Mock<IAnswerContext>>();
            _context.Setup(_personList);
            
            _peopleService = fixture.Create<PeopleService>();
        }

        [Test]
        public void Monkey()
        {
            var test = _peopleService.TestMethod(1);
            
            Assert.That(test, Is.InstanceOf<Person>());
            Assert.That(test.PersonId, Is.EqualTo(1));
        }

        [Test]
        public async void MonkeyAsync()
        {
            var test = await _peopleService.TestMethodAsync();

            Assert.That(test, Is.InstanceOf<IList<Person>>());
            Assert.That(test.Count(), Is.EqualTo(2));
        }
    }
}