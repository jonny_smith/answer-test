﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Answer.Mapper;
using Answer.Service.Entities;
using Answer.Web.Areas.Api.Colours.Models;

namespace Answer.Web.Areas.Api.Colours
{
    [RoutePrefix("api/colours")]
    public class ColourController : ApiController
    {
        private readonly ColourService _colourService;

        public ColourController(ColourService colourService)
        {
            _colourService = colourService;
        }

        [Route("")]
        public async Task<IHttpActionResult> GetColours()
        {
            var colours = await _colourService.Query<Domain.Data.Colour>()
                .OrderBy(x => x.Name)
                .ToListAsync();

            var json = colours.MapTo<IEnumerable<ColourJson>>();

            return Ok(json);
        }
    }
}