﻿namespace Answer.Web.Areas.Api.Colours.Models
{
    public class ColourJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}