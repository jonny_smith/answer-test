﻿using Answer.Mapper;

namespace Answer.Web.Areas.Api.Colours.Models
{
    public class ColourMapper : IMapper
    {
        public void Register()
        {
            AutoMapper.Mapper.CreateMap<Domain.Data.Colour, ColourJson>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ColourId));

            AutoMapper.Mapper.CreateMap<ColourJson, Domain.Data.Colour>()
                .ForMember(dest => dest.ColourId, opts => opts.MapFrom(src => src.Id))
                .Ignore(dest => dest.IsEnabled)
                .Ignore(dest => dest.People);
        }
    }
}