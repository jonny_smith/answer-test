﻿using System.Collections.Generic;
using Answer.Web.Areas.Api.Colours.Models;

namespace Answer.Web.Areas.Api.People.Models
{
    public class PersonJson
    {
        public PersonJson()
        {
            Colours = new List<ColourJson>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAuthorised { get; set; }
        public bool IsValid { get; set; }
        public bool IsEnabled { get; set; }
        public IList<ColourJson> Colours { get; set; }
    }
}