﻿using System.Collections.Generic;

namespace Answer.Web.Areas.Api.People.Models
{
    public class UpdatePersonJson
    {
        public UpdatePersonJson()
        {
            Colours = new List<int>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAuthorised { get; set; }
        public bool IsValid { get; set; }
        public bool IsEnabled { get; set; }
        public IList<int> Colours { get; set; }
    }
}