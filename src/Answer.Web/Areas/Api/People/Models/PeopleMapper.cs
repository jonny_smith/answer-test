﻿using Answer.Mapper;

namespace Answer.Web.Areas.Api.People.Models
{
    public class PeopleMapper : IMapper
    {
        public void Register()
        {
            AutoMapper.Mapper.CreateMap<Domain.Data.Person, PersonJson>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.PersonId));

            AutoMapper.Mapper.CreateMap<PersonJson, Domain.Data.Person>()
                .ForMember(dest => dest.PersonId, opts => opts.MapFrom(src => src.Id));
        }
    }
}