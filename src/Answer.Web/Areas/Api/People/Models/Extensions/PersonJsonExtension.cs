﻿using System.Collections.Generic;
using System.Linq;

namespace Answer.Web.Areas.Api.People.Models.Extensions
{
    public static class PersonJsonExtension
    {
        public static IList<PersonJson> ThenSortColours(this IList<PersonJson> list)
        {
            foreach (var item in list)
            {
                item.SortColours();
            }

            return list;
        }

        public static PersonJson ThenSortColours(this PersonJson model)
        {
            return model.SortColours();
        }

        private static PersonJson SortColours(this PersonJson model)
        {
            model.Colours = model.Colours.OrderBy(x => x.Name).ToList();
            return model;
        }
    }
}