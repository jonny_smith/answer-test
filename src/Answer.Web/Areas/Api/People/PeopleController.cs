﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Answer.Domain.Data;
using Answer.Extensions;
using Answer.Mapper;
using Answer.Service.Entities;
using Answer.Web.Areas.Api.People.Models;
using Answer.Web.Areas.Api.People.Models.Extensions;
using Answer.Web.Core;

namespace Answer.Web.Areas.Api.People
{
    [RoutePrefix("api/people")]
    public class PeopleController : ApiController
    {
        private readonly PeopleService _peopleService;

        #region .ctor
        public PeopleController(PeopleService peopleService)
        {
            _peopleService = peopleService;
        } 
        #endregion

        [Route("")]
        public async Task<IHttpActionResult> GetPeople(bool? includeColours = false)
        {
            var people = await PeopleQuery(includeColours)
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.LastName)
                .ToListAsync();

            var json = people.MapTo<IList<PersonJson>>().ThenSortColours();

            return Ok(json);
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> GetPerson(int id, bool? includeColours = false)
        {
            var people = await PeopleQuery(includeColours)
                .SingleOrDefaultAsync(x => x.PersonId == id);

            var json = people.MapTo<PersonJson>().ThenSortColours();

            return Ok(json);
        }

        [Route("")]
        public async Task<IHttpActionResult> PostPerson(PersonJson model)
        {
            var person = model.MapTo<Person>();
            var result = await _peopleService.Create(person);

            if (result.Success)
            {
                var json = result.Entity.MapTo<PersonJson>().ThenSortColours();
                return Ok(json);
            }

            ModelState.AddModelError(result.ServiceErrors);

            return BadRequest(ModelState);
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> PutPerson(PersonJson model)
        {
            var person = model.MapTo<Person>();
            var result = await _peopleService.Update(person);

            if (result.Success)
            {
                var json = result.Entity.MapTo<PersonJson>().ThenSortColours();
                return Ok(json);
            }

            ModelState.AddModelError(result.ServiceErrors);

            return BadRequest(ModelState);
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> DeletePerson(int id)
        {
            var result = await _peopleService.Delete(id);

            if (result.Success)
            {
                var json = result.Entity.MapTo<PersonJson>().ThenSortColours();
                return Ok(json);
            }

            ModelState.AddModelError(result.ServiceErrors);

            return BadRequest(ModelState);
        }

        private IQueryable<Person> PeopleQuery(bool? includeColours)
        {
            var query = _peopleService.Query<Person>();

            if (includeColours.IsTrue()) 
                query = query.Include(x => x.Colours);

            return query;
        }
    }
}