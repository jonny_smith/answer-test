﻿/// <reference path="../_references.js" />
(function () {
    'use strict';

    angular
        .module('app.core', [
            'ngRoute'
        ]);

})();