﻿/// <reference path="../_references.js" />
(function () {
    'use strict';

    angular
        .module('app', [
            'app.core',
            'app.services'
        ]);

})();