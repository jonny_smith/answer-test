﻿namespace Answer.Web
{
    public class MapperConfig
    {
        public static void RegisterMappers()
        {
            Answer.Web.Core.WebMapper.RegiserMappings();

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
