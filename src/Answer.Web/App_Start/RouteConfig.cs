﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Answer.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "BackBone",
                "backbone",
                new { controller = "Home", action = "Backbone" },
                new[] { "Web.PPM.Controllers" });

            routes.MapRoute(
                "Angular",
                "angular",
                new { controller = "Home", action = "Angular" },
                new[] { "Web.PPM.Controllers" });

            routes.MapRoute(
                "jQuery",
                "jquery",
                new { controller = "Home", action = "jQuery" },
                new[] { "Web.PPM.Controllers" });

            routes.MapRoute(
                 "Base",
                 "{*parameters}",
                 new { controller = "Home", action = "Index" },
                 new[] { "Web.PPM.Controllers" });
        }
    }
}
