﻿using System.Web.Http;
using Answer.Data;
using Answer.Web.Core.DependencyResolvers;
using Ninject;
using Ninject.Web.Common;

namespace Answer.Web
{
    public class BinderConfig
    {
        public static void RegisterDependencyResolver(HttpConfiguration config)
        {
            var kernel = new StandardKernel();

            RegisterServices(kernel);

            config.DependencyResolver = new NinjectResolver(kernel);
        }

        public static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<Domain.Data.TechTestEntities>().To<Domain.Data.TechTestEntities>().InRequestScope();
            kernel.Bind<IAnswerContext>().To<AnswerContext>().InRequestScope();
        }
    }
}
