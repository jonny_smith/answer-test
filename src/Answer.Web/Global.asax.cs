﻿using System.Web;

namespace Answer.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            Bootstrapper.Initialise();
        }
    }
}
