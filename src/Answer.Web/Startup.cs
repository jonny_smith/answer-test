﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Answer.Web.Startup))]
namespace Answer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
